const fs = require('fs');
const os = require('os');
const readline = require('readline');
const clear = require('clear');

let dictionary = {};
let count = 0;
let key = '';
let inputcount = 0;
let timestamp = Math.floor(Date.now() / 1000);

clear();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true,
  prompt: 'Add> '
});

process.stdin.setRawMode(true);

process.stdin.on('keypress', (str, key) => {
  // Save on Ctrl + S
  if (key.ctrl && key.name === 's') {
    let json = JSON.stringify(dictionary);
    // Write object to json file
    fs.writeFile(`${os.homedir()}/Documents/${timestamp}.json`, json, err => {
      if (err) throw err;
      clear();
      console.log(`File saved in ${os.homedir()}/Documents/${timestamp}.json.`);
      rl.prompt();
    });
  }
});

rl.prompt();

rl.on('line', line => {
  clear();

  console.log(`Input count: ${inputcount}\n`);
  rl.prompt();

  if (count % 2 != 0) {
    // Every 2 lines - add the pair to the dictionary
    dictionary[key] = line.trim();
  } else {
    process.stdout.write(line.trim() + ' = ');
    key = line.trim();
    dictionary[key] = '';
    inputcount++;
  }
  count++;
}).on('close', () => {
  clear();
  console.log('Bye Bye!');
  process.exit(0);
});
