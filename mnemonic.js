// TODO clean up // to arg = process.argv[2]
const fs = require('fs');
const path = require('path');
const readline = require('readline');
const chalk = require('chalk');
const clear = require('clear');
const argv = require('minimist')(process.argv.slice(2));

if (argv._[0] === undefined) {
  console.log('The path is undefined.');
  return;
}

const filePath = path.join(__dirname, argv._[0]);
//clear();
fs.readFile(filePath, 'utf8', (err, data) => {
  if (err) throw err;

  data = JSON.parse(data);

  if (argv.reverse) {
    let helper_json = {};
    // Swap json keys and values
    for (let key in data) {
      helper_json[data[key]] = key;
    }
    data = helper_json;
  }

  // Initialize the error counters
  Object.entries(data).forEach(([key, value]) => (data[key] = [value, 0]));

  // Array data to keep track of unanswered & false answered questions.
  let array_data = Object.keys(data);
  const total = array_data.length;

  cli();

  function cli() {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    let rand = Math.floor(Math.random() * array_data.length);

    // Get object's value with a random key
    let question = data[array_data[rand]][0];

    // Get question's key
    let correct_answer = Object.keys(data).find(
      key => data[key][0] === question
    );

    rl.question(`--> ${question}? \n--> `, answer => {
      clear();

      if (answer == correct_answer) {
        console.log(chalk.green('Correct answer!!'));

        // If there is no error counter - dont show the question again
        if (data[answer][1] === 0) {
          // Remove it from the array
          let index = array_data.indexOf(answer);
          array_data.splice(index, 1);

          // If there is an error counter - decrease one
        } else {
          data[answer][1]--;
        }
      } else {
        // If an error is made - increase error counter
        // Max error counter = 2
        if (data[correct_answer][1] < 2) {
          data[correct_answer][1]++;
        }
        console.log(chalk.red('Wrong answer!!'));
        console.log(`The correct answer is ${chalk.green(correct_answer)}`);
      }

      rl.close();
      console.log('---------------------------------');
      console.log(`${total - array_data.length}/${total}\n`);

      // As long there are unanswered/wrong answered questions call cli() again
      if (array_data.length != 0) {
        cli();
      } else {
        console.log('Finished.');
      }
    });
  }
});
