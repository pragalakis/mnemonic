# mnemonic

A command line tool that helps you learn a new language.

## How to run it?

* Install [Node.js](https://nodejs.org/)
* Clone the repo
* Run `npm install -g`
* Run `mnemonic your-file.json`, `mnemonic your-file.json --reverse` or `mnemonic add`
* **Learn!**

## How it works 

As an example, given the following json file

```
{
 "nein": "no",
 "ja": "yes",
 "gut": "good"
}

``` 

the program will ask you for the translated word and it will expect the original.

If you make a mistake you will be asked again at a random order.

Running the `mnemonic your-file.json --reverse` command will run the cli with reversed dictionary.

Running the `mnemonic add` command will allow you to make a dictionary.

* **Enter** to move next.
* **Ctrl + s** to save the file.

## Icons

The icon for this repo is provided by [icons8](https://icons8.com/).
